using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Calcolatrice_Gr
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
     public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        public string j;
        public string i;
        public string p;
        public double R;
        public int c1 = 0;
        public int c = 0;
      
        // b1.RaiseEvent<(new RouterEventArg(Button.clickevent);
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            // rivedere la visualizzazione con le operazioni.
            // tasto invio
            int c4=0;
            try
            {
                // non può prendere più tasti insieme ex maiusc+*   
                if (e.Key.Equals(Key.Enter))
                {
                    Invio();
                }
                if ((e.Key.Equals(Key.OemPlus) && Keyboard.Modifiers.Equals(ModifierKeys.Shift)))
                {
                    p = "*";
                    c = 1;
                    l1.Content = j + p + i;
                    c4 = 1;

                }
                if (e.Key.Equals(Key.D7) && Keyboard.Modifiers.Equals(ModifierKeys.Shift))
                {
                    p = "/";
                    c = 1;
                    l1.Content = j + p + i;
                    c4 = 1;

                }
                    if ((e.Key != Key.Enter) && (e.Key != Key.Delete) && (e.Key != Key.Back) && (e.Key != Key.OemPeriod) && (e.Key != Key.Decimal)&&(c4!=1))
                {
                    if (verifica(e) == 0)
                    {
                        Gestione(e);
                    }
                }
                // tasto cancella totale
                if (e.Key.Equals(Key.Delete))
                {
                    l1.Content = " ";
                    j = null;
                    i = null;
                    p = null;
                    c = 0;
                }
                // tasto cancella ultima cira 
                if (e.Key.Equals(Key.Back))
                {
                    Back();
                }
                if ((e.Key.Equals(Key.OemPeriod)) || (e.Key.Equals(Key.Decimal)))
                {
                    Decimale();
                }
            }catch(Exception m)
            {
                l1.Content = m.Message;
            }
        }
        private void Gestione(KeyEventArgs a)
        {
            double num = -1;
            string s;
            string j1;
            string i1;
            num = Calcolon(a.Key);
            s = Calcolotipo(a,null);
            if ((num == -1) && (s == "null"))
            {
                num = -1;
                s = null;
            }
            if (s == "null")// entrosempre 
            {
                if (c == 0)
                {
                    j1 = Convert.ToString(num);
                    j = j + j1;
                    l1.Content = j + p+i;

                }
                else
                {
                    if (num != -1)
                    {
                        i1 = Convert.ToString(num);
                        i = i + i1;
                        l1.Content = j+p+i;
                        c = 2;
                    }
                }
            }
           else
            {
                p = s;// salvo i dati nelle variabili globali 
                c = 1;// aumento il contatore 
                l1.Content = j + p + i;
            }

        }
        private string Calcolotipo(KeyEventArgs a,Button c)
        {
            if (a == null)
            {
                if (c.Equals(bp))
                    return "+";
                if (c.Equals(bm))
                    return "-";
                if (c.Equals(bpr))
                    return "*";
                if (c.Equals(bd))
                    return "/";
            }
            else
            {
                // bp somma, bm sottrazione, bpr moltiplicazione, bd divisione
                if ((a.Key.Equals(Key.Add)) || (a.Key.Equals(Key.OemPlus)))
                {
                    return "+";
                }
                if (a.Key.Equals(Key.Subtract) || (a.Key.Equals(Key.OemMinus)))
                {
                    // gestire sottrazione 
                    return "-";
                }
                if (a.Key.Equals(Key.Multiply))
                {
                    // gestione moltiplicazione 
                    return "*";
                }
                if (a.Key.Equals(Key.Divide))
                {
                    // gestire divisione 
                    return "/";
                }
            }

            return "null";
        }
        private int Calcolon(Key e)// trovo il valore 
        {
            if ((e >= Key.D0) && (e <= Key.D9))
            {
                //l1.Content = 4;

                return (int)e - 34;
            }
            if ((e >= Key.NumPad0) && (e <= Key.NumPad9))
            {
                return (int)e - 74;
            }
            return -1;
        }
        private void Binvio_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button b = new Button();
                b = (Button)sender;
                string s;
                int n = 0;
                n = Num(b);
                if (n == -1)
                {
                    if (b == binvio)
                    {
                        Invio();
                    }
                    if (b == bpun)
                    {
                        Decimale();
                    }
                    if (b == bcanc)
                    {
                        Back();
                    }
                    // bp somma, bm sottrazione, bpr moltiplicazione, bd divisione
                    if ((b == bp) || (b == bm) || (b == bpr) || (b == bd))
                    {
                        // per colpa del null non funziona
                        
                        s = Calcolotipo(null, b);
                        if (s == null)
                        {
                            s = null;
                        }
                        else
                        {
                            c = 1;
                            p = s;
                            l1.Content = j + p + i;
                        }
                    }
                }
                else
                {
                    // c=0 è il numeratore c=1 è l'operazione c=2 è il denominatore 
                    if (c == 0)// primo 
                    {
                        j = j + n;
                        l1.Content = j + p + i;
                        c = 0;
                    }
                    if ((c == 1) || (c == 2))
                    {
                        i = i + n;
                        l1.Content = j + p + i;
                        c = 2;
                    }// secondo 
                }
            }catch(Exception m)
            {
                l1.Content = m.Message;
            }

        }
        private void Invio()
        {
            double i1;
            double j1;
            
            c1 = 1;
            if ((j == null) || (i == null) || (p == null))
            {
                l1.Content = "valori nulli";
            }
            else
            {
                i1 = Convert.ToDouble(i);

                j1 = Convert.ToDouble(j);
                // asmd
                if (p == "+")
                {

                    R = j1 + i1;
                    l1.Content = R.ToString();
                }
                if (p == "-")
                {
                    R = j1 - i1;
                    l1.Content =R.ToString();
                }
                if (p == "*")
                {
                    R = j1 * i1;
                    l1.Content = R.ToString();
                }
                if (p == "/")
                {
                    R = j1 / i1;
                    l1.Content = R.ToString();
                }
            }
        }
        private void Decimale()
        {

            if (c == 0)
            {
                j = j + ",";
                l1.Content = j+p+i;
            }
            if (c == 2)
            {
                i += ",";
                l1.Content = j+p+i;
            }
        }
        private void Back()
        {
            int l = 0;
            string a = null;
            if ((i == null) && (p == null) && (j == null))
            {
                l1.Content =" ";
            }
            else
            {
                if (c1 == 1)
                {
                    l1.Content = " ";
                    R = 0;
                    i = null;
                    p = null;
                    j = null;
                    c1 = 0;
                    c = 0;
                }
                else
                {
                    if (c == 0)// entra 
                    {
                        l1.Content = "numeratore:";
                        l = j.Length;
                        if (l == 0)
                        {
                            j = "";

                        }
                        else
                        {
                            a = j.Remove(l - 1);
                            j = a;
                        }
                        l1.Content = j + p + i;


                    }
                    // farlo per il segno e per il 2 valore  segno = 1,secondo valore = 2
                    if (c == 1)
                    {
                        l1.Content = "operazione";
                        l = p.Length;
                        if (l == 1)
                        {
                            p = "";
                            c = 0;
                        }
                        else
                        {
                            a = p.Remove(l - 1);
                            p = a;
                        }
                        l1.Content = j + p + i;
                    }
                    if (c == 2)
                    {
                        l1.Content = "denominatore";
                        l = i.Length;
                        if (l == 1)
                        {
                            i = "";
                            c = 1;
                        }
                        else
                        {
                            a = i.Remove(l - 1);
                            i = a;
                        }
                        l1.Content = j + p + i;
                    }
                }
            }
            
        }
        private int Num(Button b)
        {
            if (b == b1)
            {
                return 1;
            }
            if (b == b2)
            {
                return 2;
            }
            if (b == b3)
            {
                return 3;
            }
            if (b == b4)
            {
                return 4;
            }
            if (b == b5)
            {
                return 5;
            }
            if (b == b6)
            {
                return 6;
            }
            if (b == b7)
            {
                return 7;
            }
            if (b == b8)
            {
                return 8;
            }
            if (b == b9)
            {
                return 9;
            }
            if (b == b0)
            {
                return 0;
            }
            return -1;


        }
        private int verifica(KeyEventArgs e)
        {
            int a = Calcolon(e.Key);
            string s=null;
            if (a == -1)
                s = Calcolotipo(e, null);

            if( (a == -1) && (s == "null")) 
                return -1;
            else
            {
                return 0;
            }
        }
    }
}